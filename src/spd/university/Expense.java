package spd.university;

import java.time.LocalDateTime;
import java.util.Date;

public abstract class Expense {

    private LocalDateTime createdAt;
    private int lendedBy;
    private ExpenseType type;

    private Currency currency;
    private Double amount;
}