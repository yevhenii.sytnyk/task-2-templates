package spd.university;

import java.util.Collection;

public interface Service<T Identifiable> {

    T save(T entity);

    void delete(T entity);

    Collection<T> get(Collection<Integer> ids);

    Object get(Integer id);
}