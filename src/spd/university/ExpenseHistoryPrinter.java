package spd.university;

public interface ExpenseHistoryPrinter {

    void printAccountExpenseHistory(int accountId, Sort sort);

    void printGroupExpenseHistory(Group group, Sort sort);
}