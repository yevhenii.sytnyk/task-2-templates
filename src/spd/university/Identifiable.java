package spd.university;

public interface Identifiable {

    Integer getId();
}