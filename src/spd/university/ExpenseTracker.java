package spd.university;

import java.util.Set;

public interface ExpenseTracker {

    void trackExpense(Expense expense);
    Set<Expense> getExpensesByGroup(int groupId);
    Set<Expense> getExpensesByUser(int userId);
}