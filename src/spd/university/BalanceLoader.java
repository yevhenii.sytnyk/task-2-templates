package spd.university;

public interface BalanceLoader {

    Balance getUserBalance(Group group, Account account);

    Balance getUserBalance(Account account);
}